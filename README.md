# SPA-Vue-Example

## Project setup
```
npm install
npm install -g json-server
```

### Start Json server
```
json-server --watch db.json
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
